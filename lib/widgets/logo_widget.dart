import 'package:flutter/material.dart';

class LogoWidget extends StatelessWidget {
  const LogoWidget({
    Key? key,
    this.height = double.infinity,
    this.width = double.infinity,
  }) : super(key: key);

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned.fill(
            child: ColorFiltered(
              colorFilter: ColorFilter.mode(
                  Theme.of(context).primaryColor.withRed(150),
                  BlendMode.modulate),
              child: Image.asset(
                "assets/images/logo_glow_white_256x256.png",
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
          Positioned.fill(
            child: ColorFiltered(
              colorFilter: ColorFilter.mode(
                  Theme.of(context).primaryColor, BlendMode.modulate),
              child: Image.asset(
                "assets/images/logo_white_256x256.png",
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
