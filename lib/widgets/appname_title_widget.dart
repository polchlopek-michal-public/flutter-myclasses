import 'package:flutter/material.dart';

class AppnameTitleWidget extends StatelessWidget {
  const AppnameTitleWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        "MyClasses",
        overflow: TextOverflow.visible,
        softWrap: false,
        style: Theme.of(context)
            .textTheme
            .headline1!
            .copyWith(fontSize: 60),
      ),
    );
  }
}
