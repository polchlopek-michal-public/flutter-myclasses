import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myclassesapp/blocs/session_bloc.dart';

class SigninWidget extends StatelessWidget {
  SigninWidget({Key? key}) : super(key: key);
  final TextEditingController loginController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Form(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              decoration: InputDecoration(labelText: "username"),
              controller: loginController,
            ),
            SizedBox(height: 20),
            TextFormField(
              obscureText: true,
              decoration: InputDecoration(labelText: "password"),
              controller: passwordController,
            ),
            SizedBox(height: 20),
            ElevatedButton(
                onPressed: () => formSubmission(context),
                child: Text("Sign in!"))
          ],
        ),
      ),
    );
  }

  void formSubmission(BuildContext context) {
    String login = loginController.text;
    String password = passwordController.text;

    // TODO: validation...
    BlocProvider.of<SessionBloc>(context).startSession(
      login: login,
      password: password,
    );
  }
}
