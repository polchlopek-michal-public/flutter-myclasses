import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myclassesapp/blocs/registration_bloc.dart';

class SignupWidget extends StatefulWidget {
  const SignupWidget({Key? key}) : super(key: key);

  @override
  _SignupWidgetState createState() => _SignupWidgetState();
}

class _SignupWidgetState extends State<SignupWidget> {
  bool conditionAccepted = false;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordConfirmationController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextFormField(
            controller: emailController,
            decoration: InputDecoration(labelText: "e-mail"),
          ),
          SizedBox(height: 20),
          TextFormField(
            obscureText: true,
            controller: passwordController,
            decoration: InputDecoration(labelText: "password"),
          ),
          SizedBox(height: 20),
          TextFormField(
            obscureText: true,
            controller: passwordConfirmationController,
            decoration: InputDecoration(labelText: "repeat password"),
          ),
          SizedBox(height: 20),
          _buildAgreementConfirmationBox(context),
          SizedBox(height: 60),
          ElevatedButton(
              onPressed: () => formSubmission(context), child: Text("Sign up!"))
        ],
      ),
    );
  }

  Widget _buildAgreementConfirmationBox(BuildContext context) {
    return Row(children: [
      Checkbox(
          value: conditionAccepted,
          onChanged: (bool) {
            setState(() {
              conditionAccepted = !conditionAccepted;
            });
          }),
      Flexible(
          child: Text(
        "Akceptuję warunki umowy korzystania z aplikacji MyClasses",
      ))
    ]);
  }

  void formSubmission(BuildContext context) {
    String email = emailController.text;
    String password = passwordController.text;
    String passwordConfirmation = passwordConfirmationController.text;

    // TODO: validation...
    BlocProvider.of<RegistrationBloc>(context).registerUser(
      login: email,
      password: password,
      passwordConfirmation: passwordConfirmation,
    );
  }
}
