import 'package:flutter/material.dart';
import 'package:myclassesapp/utils/non_nullable_list_converter.dart';

class ElevatedButtonWithIcon extends StatelessWidget {
  const ElevatedButtonWithIcon({
    Key? key,
    Widget? icon,
    Widget? child,
    double iconSize = 20,
    required void Function() onPressed,
  })  : __icon = icon,
        __child = child,
        __onPressed = onPressed,
        __iconSize = iconSize,
        super(key: key);

  final Widget? __icon;
  final Widget? __child;
  final void Function() __onPressed;
  final double __iconSize;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: __onPressed,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: NonNullableListConverter.convert<Widget>([
          __child,
          SizedBox(width: 20),
          Container(
            child: __icon,
            width: __iconSize,
            height: __iconSize,
          ),
        ]),
      ),
    );
  }
}
