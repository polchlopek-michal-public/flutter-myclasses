import 'package:flutter/material.dart';

class WebAppbarClipper extends CustomClipper<Path> {
  WebAppbarClipper(
      {required this.width,
      required this.height,
      required this.inflectionBoxSize});

  final double width;
  final double height;
  final double inflectionBoxSize;

  // TODO: Redundant code - the same as in appbar background painter - needs to be extraceted
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(0,0);
    path.lineTo(0, height);
    path.lineTo(width/2 - inflectionBoxSize*2, height);
    path.lineTo(width/2, height + inflectionBoxSize*2);
    path.lineTo(width/2 + inflectionBoxSize*2, height);
    path.lineTo(width, height);
    path.lineTo(width, 0);
    path.lineTo(0,0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}
