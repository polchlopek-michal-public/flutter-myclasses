import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:myclassesapp/widgets/web_appbar_widget/web_appbar_background_painter.dart';
import 'package:myclassesapp/widgets/web_appbar_widget/web_appbar_button_widget.dart';
import 'package:myclassesapp/widgets/logo_widget.dart';
import 'package:myclassesapp/widgets/web_appbar_widget/web_appbar_clipper.dart';

enum WebAbbarActiveState {
  NONE,
  APP_NAME,
  CLUBS,
  ABOUT_US,
  OFFERS,
  SIGN_IN,
  SIGN_UP
}

class WebAppbarWidget extends StatefulWidget implements PreferredSizeWidget {
  WebAppbarWidget({Key? key})
      : preferredSize = Size.fromHeight(80),
        super(key: key);

  @override
  final Size preferredSize;

  @override
  _WebAppbarWidgetState createState() => _WebAppbarWidgetState();
}

class _WebAppbarWidgetState extends State<WebAppbarWidget> {
  WebAbbarActiveState activeButton = WebAbbarActiveState.APP_NAME;

  static const double LOGO_SIZE = 100;
  static const double INFLECTION = 20;
  static const double OVER_INFLECTION = 30;

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: WebAppbarClipper(
        height: widget.preferredSize.height + 30,
        width: MediaQuery.of(context).size.width,
        inflectionBoxSize: OVER_INFLECTION,
      ),
      child: Stack(
        children: [
          buildBackgroundLayer(
            context,
            color: Theme.of(context).primaryColor,
            height: widget.preferredSize.height,
            width: MediaQuery.of(context).size.width,
            inflection: INFLECTION,
          ),
          BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
            child: Stack(
              children: [
                buildBackgroundLayer(
                  context,
                  color: HSVColor.fromColor(Theme.of(context).backgroundColor)
                      .withValue(0.14)
                      .toColor(),
                  height: widget.preferredSize.height,
                  width: MediaQuery.of(context).size.width,
                  inflection: INFLECTION,
                ),
                buildInterfaceLayer(context),
              ],
            ),
          ),
        ],
      ),
    );
  }

  // layer builders:
  Widget buildAppbarLayer({required context, required Widget child}) {
    return Container(
        height: widget.preferredSize.height,
        alignment: Alignment.center,
        child: child);
  }

  Widget buildInterfaceLayer(BuildContext context) {
    return buildAppbarLayer(
      context: context,
      child: Container(
        child: Stack(
          alignment: Alignment.center,
          clipBehavior: Clip.none,
          children: [
            Positioned(top: 10, child: _buildLogoWidget(context)),
            Positioned(left: 0, child: buildLeftPanel(context)),
            Positioned(right: 0, child: buildRightPanel(context)),
          ],
        ),
      ),
    );
  }

  Widget buildBackgroundLayer(
    BuildContext context, {
    required double width,
    required double height,
    Color color = Colors.black,
    required double inflection,
  }) {
    return buildAppbarLayer(
      context: context,
      child: Container(
        child: CustomPaint(
          painter: WebappbarBackgroundPainter(
            basicHight: height,
            basicWidth: width,
            inflectionBoxSize: LOGO_SIZE + inflection,
            color: color,
          ),
        ),
      ),
    );
  }

  // builders:
  Widget buildLeftPanel(BuildContext context) {
    return Row(
      children: [
        WebAppbarButtonWidget(
            onPressed: () => navigateTo("/index-screen"),
            child: Text("MyClasses"),
            underlined: activeButton == WebAbbarActiveState.APP_NAME),
        WebAppbarButtonWidget(
            onPressed: () {},
            child: Text("Clubs"),
            underlined: activeButton == WebAbbarActiveState.CLUBS),
        WebAppbarButtonWidget(
            onPressed: () {},
            child: Text("About us"),
            underlined: activeButton == WebAbbarActiveState.ABOUT_US),
        WebAppbarButtonWidget(
            onPressed: () {},
            child: Text("Pricing"),
            underlined: activeButton == WebAbbarActiveState.OFFERS),
      ],
    );
  }

  Widget buildRightPanel(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        // ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.language), label: Text("Language"),),
        WebAppbarButtonWidget(
          onPressed: () {}, // TODO: redundant !!!
          child: ElevatedButton(
            onPressed: () => navigateTo("/authentication-screen"),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Sign in"),
            ),
          ),
        ),
        WebAppbarButtonWidget(
          onPressed: () {}, // TODO: redundant !!!
          child: OutlinedButton(
            onPressed: () => navigateTo("/authentication-screen"),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Sign up"),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildLogoWidget(BuildContext context) {
    return GestureDetector(
        onPanEnd: (_) => navigateTo("/index-screen"),
        child: LogoWidget(height: LOGO_SIZE, width: LOGO_SIZE));
  }

  // helpers

  void navigateTo(String route) {
    if (ModalRoute.of(context)!.settings.name != route) {
      Navigator.of(context).pushNamed(route);
    }
  }
}
