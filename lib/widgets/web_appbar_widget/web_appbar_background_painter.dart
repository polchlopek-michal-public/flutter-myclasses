import 'package:flutter/material.dart';

class WebappbarBackgroundPainter extends CustomPainter {
  WebappbarBackgroundPainter(
      {required this.basicHight,
      required this.basicWidth,
      required this.inflectionBoxSize,
      required this.color});

  final double basicHight;
  final double basicWidth;
  final double inflectionBoxSize;
  Color color;

  final Paint painter = Paint();

  @override
  void paint(Canvas canvas, Size size) {
    painter.color = color;
    canvas.drawRect(
        Rect.fromPoints(Offset(-basicWidth, -basicHight),
            Offset(basicWidth, basicHight / 2)),
        painter);
    canvas.drawPath(_getInflectionPath(), painter);
  }

  // TODO: this dunction has an repetition - needs to be extracted
  Path _getInflectionPath() {
    return Path()
      ..moveTo(-inflectionBoxSize / 2, basicHight / 2)
      ..lineTo(0, basicHight + inflectionBoxSize / 4)
      ..lineTo(inflectionBoxSize / 2, basicHight / 2)
      ..lineTo(-inflectionBoxSize / 2, basicHight / 2);
  }

  @override
  bool shouldRepaint(WebappbarBackgroundPainter oldDelegate) => false;

  @override
  bool shouldRebuildSemantics(WebappbarBackgroundPainter oldDelegate) => false;
}
