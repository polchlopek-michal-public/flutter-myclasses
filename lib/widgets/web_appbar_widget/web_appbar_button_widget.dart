import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class WebAppbarButtonWidget extends StatefulWidget {
  const WebAppbarButtonWidget({
    required this.child,
    Key? key,
    required this.onPressed,
    this.underlined = false,
  }) : super(key: key);

  final Widget child;
  final underlined;
  final void Function() onPressed;

  @override
  _WebAppbarButtonWidgetState createState() => _WebAppbarButtonWidgetState();
}

class _WebAppbarButtonWidgetState extends State<WebAppbarButtonWidget>
    with SingleTickerProviderStateMixin {
  // scale animation:
  static const Duration SCALE_ANIMATION_DURATION = Duration(milliseconds: 200);
  static const double DEFAULT_SCALE = 1.0;
  static const double MAX_SCALE = 1.25;
  late AnimationController scaleAnimationController;
  late Animation<double> scaleAnimation;

  @override
  void initState() {
    super.initState();
    scaleAnimationController =
        AnimationController(vsync: this, duration: SCALE_ANIMATION_DURATION);
    scaleAnimation = Tween<double>(begin: DEFAULT_SCALE, end: MAX_SCALE)
        .animate(scaleAnimationController);
    scaleAnimation.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanEnd: (DragEndDetails details) => widget.onPressed(),
      child: MouseRegion(
        onEnter: _onMouseEntered,
        onExit: _onMouseExited,
        child: Transform.scale(
          scale: scaleAnimation.value,
          child: Container(
            width: 170,
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(vertical: 40, horizontal: 30),
            child: Column(
              children: [
                widget.child,
                widget.underlined ? buildUnderline(context) : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildUnderline(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 2,
          color: Theme.of(context).primaryColor,
        ),
        BackdropFilter(filter: ImageFilter.blur(sigmaX: 100, sigmaY: 100)),
      ],
    );
  }

  void _onMouseEntered(PointerEnterEvent event) {
    scaleAnimationController.forward();
  }

  void _onMouseExited(PointerExitEvent event) {
    scaleAnimationController.reverse();
  }
}
