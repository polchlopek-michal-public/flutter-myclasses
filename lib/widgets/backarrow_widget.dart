import 'package:flutter/material.dart';
import 'package:myclassesapp/theme/style.dart';

class BackarrowWidget extends StatelessWidget {
  const BackarrowWidget(
      {Key? key, required this.child, required this.onBackArrowClicked})
      : super(key: key);

  final Widget child;
  final void Function() onBackArrowClicked;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          buildBackArrowWidget(context),
          child,
        ],
      ),
    );
  }

  Widget buildBackArrowWidget(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      child: IconButton(
        padding: EdgeInsets.all(0),
        onPressed: onBackArrowClicked,
        icon: Icon(
          Icons.arrow_back,
          color: MyClassesColors.textColor,
        ),
      ),
    );
  }
}
