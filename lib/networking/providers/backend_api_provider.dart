import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:myclassesapp/errors/networking_errors.dart';

class BackendApiProvider {
  const BackendApiProvider({required this.address, required this.port});
  final String address;
  final int port;

  Future<dynamic> httpGet({required String endpoint}) async {
    // TODO...
  }

  Future<dynamic> httpPost({
    required String endpoint,
    required String body,
  }) async {
    // Performing post and receiving:
    http.Response response = await http.post(

      Uri.parse("$address:$port$endpoint"),
      body: body,
    );

    // Interpreting repsponse:
    switch (response.statusCode) {
      case 200:
        return json.decode(response.body.toString());
      case 400:
        throw NetworkingBadRequestError(
            "Bad request exception: ${response.request.toString()}");
      case 401:
      case 403:
        throw NetworkingUnauthorizedError(
            "Unauthorized request: ${response.request.toString()}");
      default:
        throw NetworkingBadRequestError(
            "Bad request exception: ${response.request.toString()}");
    }
  }
}
