enum ResponseStatus {
  LOADING,
  COMPLETED,
  ERROR,
}

class Response<T> {
  Response.loading({required String message})
      : status = ResponseStatus.LOADING,
        _message = message;
  Response.completed({required T data})
      : status = ResponseStatus.LOADING,
        _data = data;
  Response.error({required String message})
      : status = ResponseStatus.LOADING,
        _message = message;

  String get message => _message == null ? "" : _message!;
  T get data => _data == null ? throw Exception() : _data!;

  final ResponseStatus status;
  T? _data;
  String? _message;
}
