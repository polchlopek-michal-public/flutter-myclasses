class RegistrationResponse {
  RegistrationResponse({
    this.success = false,
    this.errors = const [],
    this.data = const [],
    this.auth = false,
  });

  bool success;
  List<dynamic> errors;
  List<dynamic> data;
  bool auth;
}
