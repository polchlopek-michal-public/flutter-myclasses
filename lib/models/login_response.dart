class LoginResponse {
  LoginResponse({
    this.auth = false,
    this.errors = const [],
    this.token = "",
    this.success = false,
  });

  bool success;
  bool auth;
  List<dynamic> errors;
  String token;
}
