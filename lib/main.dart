import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myclassesapp/blocs/registration_bloc.dart';
import 'package:myclassesapp/blocs/session_bloc.dart';
import 'package:myclassesapp/config/config_provider.dart';
import 'package:myclassesapp/networking/providers/backend_api_provider.dart';
import 'package:myclassesapp/repositories/user_repository.dart';
import 'package:myclassesapp/routes.dart';
import 'package:myclassesapp/theme/style.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

void main() {
  runApp(MyClassesApp());
}

class MyClassesApp extends StatelessWidget {
  MyClassesApp();
  final ThemeData theme = MyClassesStyle.theme();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: ConfigProvider.initialize(context),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return LoadingApp(theme: theme);
          case ConnectionState.done:
            return _buildApplication(context);
          case ConnectionState.none:
          case ConnectionState.active:
          default:
            return LoadingApp(theme: theme);
        }
      },
    );
  }

  Widget _buildApplication(BuildContext context) {
    // config:
    ConfigProvider config = ConfigProvider.provide();

    // Create application of required platform:
    late Widget application;
    if (kIsWeb) {
      application = WebApp(theme: theme);
    } else {
      application = MobileApp(theme: theme);
    }

    // Providers:
    BackendApiProvider backend = BackendApiProvider(
        address: config.backend().address, port: config.backend().port);

    // Build framework:
    return MultiBlocProvider(
      providers: [
        BlocProvider<SessionBloc>(create: (BuildContext context) {
          return SessionBloc(
            userRepository: UserRepository(backend: backend),
          );
        }),
        BlocProvider<RegistrationBloc>(create: (BuildContext context) {
          return RegistrationBloc(
            userRepository: UserRepository(backend: backend),
          );
        })
      ],
      child: application,
    );
  }
}

class MobileApp extends StatelessWidget {
  const MobileApp({Key? key, required this.theme}) : super(key: key);
  final ThemeData theme;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: theme,
      routes: mobileRoutes,
      initialRoute: '/authentication-screen',
    );
  }
}

class WebApp extends StatelessWidget {
  const WebApp({Key? key, required this.theme}) : super(key: key);
  final ThemeData theme;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: theme,
      routes: webRoutes,
      initialRoute: '/index-screen',
    );
  }
}

class LoadingApp extends StatelessWidget {
  const LoadingApp({Key? key, required this.theme}) : super(key: key);
  final ThemeData theme;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: theme,
      home: Scaffold(
        body: Container(),
      ),
    );
  }
}
