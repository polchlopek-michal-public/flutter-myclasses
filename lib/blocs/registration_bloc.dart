import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myclassesapp/managers/session_manager/session_manager.dart';
import 'package:myclassesapp/models/registration_response.dart';
import 'package:myclassesapp/repositories/user_repository.dart';

class RegistrationBloc extends Cubit<RegistrationResponse> {
  RegistrationBloc({required this.userRepository})
      : super(RegistrationResponse());
  final UserRepository userRepository;
  final SessionManager sessionManager = SessionManager();

  void registerUser(
      {required String login,
      required String password,
      required String passwordConfirmation}) {
    userRepository
        .registerUser(
          login: login,
          password: password,
          passwordConfirmation: passwordConfirmation,
        )
        .then(
          (RegistrationResponse registrationResponse) =>
              onUserRegistered(login, registrationResponse),
        )
        .timeout(
          Duration(milliseconds: 300),
        );
  }

  void onUserRegistered(
    String login,
    RegistrationResponse registrationResponse,
  ) {
    // TODO: handle failures...
    print('User registration perfomed "$registrationResponse"');
  }
}
