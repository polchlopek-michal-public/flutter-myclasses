import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myclassesapp/managers/session_manager/session_manager.dart';
import 'package:myclassesapp/managers/session_manager/session_opening_error_reason.dart';
import 'package:myclassesapp/models/login_response.dart';
import 'package:myclassesapp/repositories/user_repository.dart';

class SessionBloc extends Cubit<SessionManager> {
  SessionBloc({required this.userRepository}) : super(SessionManager());
  final UserRepository userRepository;

  void startSession({required String login, required String password}) {
    state.sessionStartPending();
    emit(state);
    userRepository
        .authenticateUser(login: login, password: password)
        .then((LoginResponse user) => onSessionStartPerformed(login, user));
  }

  void onSessionStartPerformed(String login, LoginResponse loginResponse) {
    if (loginResponse.success) {
      state.startSession(login, loginResponse.token);
    } else {
      state.onSessionStartFailed(SessionOpeningErrorReason.INCORRECT_INPUT);
    }
    emit(state);
  }

    void closeSession() {
    state.closeSession();
    emit(state);
  }
}
