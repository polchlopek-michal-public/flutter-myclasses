import 'package:myclassesapp/errors/session_error.dart';
import 'package:myclassesapp/managers/session_manager/session_opening_error_reason.dart';
import 'package:myclassesapp/managers/session_manager/session_status.dart';

/// Manges session on client's side.
class SessionManager {
  SessionStatus _status = SessionStatus.CLOSED;

  // common:
  String _email = "";

  // Session opened:
  String _token = "";

  // session opening failed:
  SessionOpeningErrorReason _openingErrorReason =
      SessionOpeningErrorReason.NONE;

  // getters:
  SessionStatus status() {
    return _status;
  }

  String email() {
    return _email;
  }

  String token() {
    if (_status != SessionStatus.OPENED) {
      throw IncorrectSessionStatusError(
          actual: _status, expected: SessionStatus.OPENED);
    }
    return _token;
  }

  SessionOpeningErrorReason openingErrorReason() {
    if (_status != SessionStatus.OPENING_FAILED) {
      throw IncorrectSessionStatusError(
          actual: _status, expected: SessionStatus.OPENING_FAILED);
    }
    return _openingErrorReason;
  }

  // actions:
  void startSession(String token, String email) {
    _status = SessionStatus.OPENED;
    _token = token;
    _email = email;
  }

  void onSessionStartFailed(SessionOpeningErrorReason reason) {
    _status = SessionStatus.OPENING_FAILED;
    _openingErrorReason = reason;
  }

  void sessionStartPending() {
    _status = SessionStatus.OPENING_PENDING;
  }

  void closeSession() {
    _status = SessionStatus.CLOSED;
    _token = "";
    _email = "";
  }
}
