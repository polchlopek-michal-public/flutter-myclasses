import 'package:flutter/widgets.dart';
import 'package:myclassesapp/screens/mobile/authentication_mobile_screen/authentication_mobile_screen.dart';
import 'package:myclassesapp/screens/web/index_web_screen.dart';
import 'package:myclassesapp/screens/web/authentication_web_screen/authentication_web_screen.dart';

final Map<String, WidgetBuilder> mobileRoutes = <String, WidgetBuilder>{
  // mobile routes
  "/authentication-screen": (BuildContext context) =>
      AuthenticationMobileScreen(),
};

final Map<String, WidgetBuilder> webRoutes = <String, WidgetBuilder>{
  "/index-screen": (BuildContext context) => IndexWebScreen(),
  "/authentication-screen": (BuildContext context) =>
      AuthenticationWebScreen(),
};
