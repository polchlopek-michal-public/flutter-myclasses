import 'package:flutter/material.dart';

class MyClassesColors {
  static Color primaryColor = Color(0xFF8F4444);
  static Color accentColor = Color(0xFF9E4B4B);
  static Color backgroundColor = Color(0xFF2E2E2E);
  static Color textColor = Color(0xFFFFFFFF);
}

class MyClassesGradients {
  static Gradient backgroundGradient() {
    return LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: [
        MyClassesColors.backgroundColor,
        MyClassesColors.primaryColor,
      ],
    );
  }
}

/// Theme data factory method

class MyClassesStyle {
  static ThemeData theme() {
    return ThemeData(
        scaffoldBackgroundColor: MyClassesColors.backgroundColor,
        primaryColor: MyClassesColors.primaryColor,
        accentColor: MyClassesColors.accentColor,
        backgroundColor: MyClassesColors.backgroundColor,
        outlinedButtonTheme: __outlinedButtonThemeData(),
        elevatedButtonTheme: __elevatedButtonThemeData(),
        textTheme: __textTheme(),
        inputDecorationTheme: __inputDecorationTheme(),
        checkboxTheme: __checboxThemeData());
  }

  static ElevatedButtonThemeData __elevatedButtonThemeData() {
    return ElevatedButtonThemeData(
      style: ButtonStyle(
        shape: MaterialStateProperty.all<OutlinedBorder>(RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
        )),
        foregroundColor:
            MaterialStateProperty.all<Color>(MyClassesColors.textColor),
        backgroundColor:
            MaterialStateProperty.all<Color>(MyClassesColors.accentColor),
      ),
    );
  }

  static OutlinedButtonThemeData __outlinedButtonThemeData() {
    return OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        primary: Colors.white, // primary is the color of button's text
        side: BorderSide(
            color: MyClassesColors.accentColor,
            width: 3,
            style: BorderStyle.solid),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
      ),
    );
  }

  static InputDecorationTheme __inputDecorationTheme() {
    return InputDecorationTheme(
      contentPadding: EdgeInsets.all(10),
      hintStyle: __hintTextStyle(),
      labelStyle: __inputLabelTextStyle(),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        borderSide: BorderSide(
            color: MyClassesColors.primaryColor,
            width: 3.0,
            style: BorderStyle.solid),
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        borderSide: BorderSide(
            color: MyClassesColors.accentColor,
            width: 3.0,
            style: BorderStyle.solid),
      ),
    );
  }

  static CheckboxThemeData __checboxThemeData() {
    return CheckboxThemeData(
      fillColor: MaterialStateProperty.all<Color>(MyClassesColors.accentColor),
    );
  }

  static TextTheme __textTheme() {
    return TextTheme(
      headline1: __whiteHeadlineTextStyle(),
      headline2: __whiteTextStyle(),
      headline3: __whiteTextStyle(),
      headline4: __whiteTextStyle(),
      headline5: __whiteTextStyle(),
      headline6: __whiteTextStyle(),
      bodyText1: __whiteBody1TextStyle(),
      bodyText2: __whiteTextStyle(),
      button: __whiteTextStyle(),
      subtitle1: __whiteTextWithShadowStyle(), // textfields and inputs
    );
  }

  static TextStyle __whiteTextStyle() {
    return TextStyle(
      color: Colors.white,
      fontFamily: "HemiHead",
      fontSize: 18,
    );
  }

    static TextStyle __whiteBody1TextStyle() {
    return TextStyle(
      color: Colors.white,
      fontFamily: "HemiHead",
    );
  }


  static TextStyle __whiteHeadlineTextStyle() {
    return TextStyle(
        color: Colors.white,
        fontFamily: "HemiHead",
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.italic);
  }

  static TextStyle __whiteTextWithShadowStyle() {
    return TextStyle(color: Colors.white, shadows: [
      Shadow(blurRadius: 1, color: Colors.black, offset: Offset(1, 1))
    ]);
  }

  static TextStyle __hintTextStyle() {
    return TextStyle(
      color: Colors.white38,
    );
  }

  static TextStyle __inputLabelTextStyle() {
    return TextStyle(color: Colors.white, fontSize: 15, shadows: [
      Shadow(offset: Offset(2, 2), blurRadius: 5.0, color: Colors.black)
    ]);
  }

}
