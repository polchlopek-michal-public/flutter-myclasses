import 'package:flutter/material.dart';

class StyleModifier {
  static TextStyle textHeadlineWithBackgroundColor(BuildContext context) {
    return Theme.of(context)
        .textTheme
        .headline1!
        .copyWith(color: Theme.of(context).backgroundColor);
  }

  static TextStyle textBodyWithBackgroundColor(BuildContext context) {
    return Theme.of(context)
        .textTheme
        .bodyText2!
        .copyWith(color: Theme.of(context).backgroundColor);
  }
}
