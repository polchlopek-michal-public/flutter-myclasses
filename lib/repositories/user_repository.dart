import 'package:myclassesapp/models/login_response.dart';
import 'package:myclassesapp/models/registration_response.dart';
import 'package:myclassesapp/networking/providers/backend_api_provider.dart';

class UserRepository {
  UserRepository({required this.backend});
  final BackendApiProvider backend;

  /// Authenticates user with passed [login] and [password]
  /// The result of authentication is [LoginResponse] model,
  /// When authentiction finishes with success, [LoginResponse] should hold
  /// security token, else [LoginResponse] will hold errors.
  Future<LoginResponse> authenticateUser({
    required String login,
    required String password,
  }) async {
    dynamic response = await backend.httpPost(
      endpoint: "/api/v1/login",
      body: '{ "login": "$login", "password": "$password" }',
    );
    return LoginResponse(
      success: response["success"],
      errors: response["errors"],
      token: response["data"]["token"],
      auth: response["auth"],
    );
  }

  /// Registers new user.
  Future<RegistrationResponse> registerUser({
    required String login,
    required String password,
    required String passwordConfirmation,
  }) async {
    dynamic response = await backend.httpPost(
      endpoint: "/api/v1/register",
      body:
          '{ "login": "$login", "password": "$password", "passwordConfirmation": "$passwordConfirmation" }',
    );
    return RegistrationResponse(
      success: response["success"],
      errors: response["errors"],
      data: response["data"],
      auth: response["auth"],
    );
  }
}
