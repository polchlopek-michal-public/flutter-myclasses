import 'package:flutter/material.dart';

enum KeyboardVisibilityStatus { VISIABLE, INVISIABLE }

/// Controller of keyboard visibility.
/// Allows to hide keyboard and invoke [onKeyboardDismiss()] callback.
/// Responsible for detecting keyboard revelation,
/// when detected [onKeyboardReveal()] callback is invoked.
/// NOTES:
/// For fully independent functionality [KeyboardVisibilityController]
/// should also detect keyboard dismissing, but for better efficiency
/// an architectural decision was made to use public function [hideKeyboard()] instead.
class KeyboardVisibilityController with ChangeNotifier {
  KeyboardVisibilityController({
    required BuildContext context,
    required this.onKeyboardReveal,
    required this.onKeyboardDismiss,
  }) : _context = context {
    Future<void>(_visibilityWatchdog);
  }
  static const VISIBILITY_WATHCDOG_DURATION = Duration(milliseconds: 20);
  BuildContext _context;
  bool _isWatchdogRunning = true;
  KeyboardVisibilityStatus _status = KeyboardVisibilityStatus.INVISIABLE;
  void Function() onKeyboardReveal;
  void Function() onKeyboardDismiss;

  /// Hides keyboard.
  /// [onKeyboardDismiss()] callback will be invoked
  /// when watchdog will detect dismiss.
  void hideKeyboard() {
    if (isVisible()) {
      FocusScope.of(_context).requestFocus(FocusNode());
    }
  }

  bool isVisible() {
    return _status == KeyboardVisibilityStatus.VISIABLE;
  }

  /// Watchdog action.
  /// Invokes [onKeyboardDismiss()] when keyboard dismisses and
  /// [onKeyboardReveal()] when keyboard reveals.
  void _visibilityWatchdog() {
    if (!isVisible() && MediaQuery.of(_context).viewInsets.bottom != 0.0) {
      _changeStatus();
      onKeyboardReveal();
    }
    if (isVisible() && MediaQuery.of(_context).viewInsets.bottom == 0.0) {
      _changeStatus();
      onKeyboardDismiss();
    }
    Future.delayed(VISIBILITY_WATHCDOG_DURATION).then((_) {
      if (_isWatchdogRunning) {
        Future(_visibilityWatchdog);
      }
    });
  }

  void _changeStatus() {
    if (_status == KeyboardVisibilityStatus.INVISIABLE) {
      _status = KeyboardVisibilityStatus.VISIABLE;
    } else {
      _status = KeyboardVisibilityStatus.INVISIABLE;
    }
  }

  @override
  void dispose() {
    _isWatchdogRunning = false;
    super.dispose();
  }
}
