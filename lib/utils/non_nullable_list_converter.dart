/// Removes nulls from list passed as a paramter
/// and return modyfied list without nulls.
class NonNullableListConverter {
  static convert<T>(List<T?> list) {
    List<T> retList = [];
    list.forEach((e) {
      if (e != null) retList.add(e);
    });
    return retList;
  }
}
