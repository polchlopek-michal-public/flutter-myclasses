import 'package:flutter/material.dart';
import 'package:myclassesapp/screens/web/authentication_web_screen/widgets/signin_text_area_widget.dart';
import 'package:myclassesapp/screens/web/authentication_web_screen/widgets/signup_text_area_widget.dart';
import 'package:myclassesapp/widgets/authentication/sigin_widget.dart';
import 'package:myclassesapp/widgets/authentication/signup_widget.dart';
import 'package:myclassesapp/widgets/web_appbar_widget/web_appbar_widget.dart';

enum AuthenticationWebScreenMode {
  NONE,
  SIGNIN_MODE,
  SIGNUP_MODE,
}

class AuthenticationWebScreen extends StatefulWidget {
  AuthenticationWebScreen({Key? key}) : super(key: key);

  final PreferredSizeWidget appBarWidget = WebAppbarWidget();

  @override
  _AuthenticationWebScreenState createState() =>
      _AuthenticationWebScreenState();
}

class _AuthenticationWebScreenState extends State<AuthenticationWebScreen>
    with TickerProviderStateMixin {
  static const Duration MODE_CHANGE_ANIMATIONS_DURATION =
      Duration(milliseconds: 300);

  AuthenticationWebScreenMode mode = AuthenticationWebScreenMode.SIGNIN_MODE;

  late Animation primaryColoredPlatePositionAnimation;
  late AnimationController primaryColoredPlatePositionAnimationController;

  late Animation signinTextAreaWidgetOpacityAnimation;
  late AnimationController signinTextAreaWidgetOpacityAnimationControler;

  late Animation signinTextAreaWidgetPositionAnimation;
  late AnimationController signinTextAreaWidgetPositionAnimationControler;

  late Animation signinWidgetOpacityAnimation;
  late AnimationController signinWidgetOpacityAnimationControler;

  late Animation signupTextAreaWidgetOpacityAnimation;
  late AnimationController signupTextAreaWidgetOpacityAnimationControler;

  late Animation signupTextAreaWidgetPositionAnimation;
  late AnimationController signupTextAreaWidgetPositionAnimationControler;

  late Animation signupWidgetOpacityAnimation;
  late AnimationController signupWidgetOpacityAnimationControler;

  @override
  void initState() {
    // primary colored plate animation:
    primaryColoredPlatePositionAnimationController = AnimationController(
        vsync: this, duration: MODE_CHANGE_ANIMATIONS_DURATION);

    primaryColoredPlatePositionAnimation = Tween<double>(begin: 0, end: 0.5)
        .animate(primaryColoredPlatePositionAnimationController)
          ..addListener(update);

    // signin text area animation:

    signinTextAreaWidgetOpacityAnimationControler = AnimationController(
        vsync: this, duration: MODE_CHANGE_ANIMATIONS_DURATION);

    signinTextAreaWidgetOpacityAnimation = Tween<double>(begin: 1.0, end: 0.0)
        .animate(signinTextAreaWidgetOpacityAnimationControler)
          ..addListener(update);

    signinTextAreaWidgetPositionAnimationControler = AnimationController(
        vsync: this, duration: MODE_CHANGE_ANIMATIONS_DURATION);

    signinTextAreaWidgetPositionAnimation = Tween<double>(begin: 0, end: 0.5)
        .animate(signinTextAreaWidgetPositionAnimationControler)
          ..addListener(update);

    // signup text area animation:

    signupTextAreaWidgetOpacityAnimationControler = AnimationController(
        vsync: this, duration: MODE_CHANGE_ANIMATIONS_DURATION);

    signupTextAreaWidgetOpacityAnimation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(signupTextAreaWidgetOpacityAnimationControler)
          ..addListener(update);

    signupTextAreaWidgetPositionAnimationControler = AnimationController(
        vsync: this, duration: MODE_CHANGE_ANIMATIONS_DURATION);

    signupTextAreaWidgetPositionAnimation = Tween<double>(begin: 0.5, end: 0.0)
        .animate(signupTextAreaWidgetPositionAnimationControler)
          ..addListener(update);

    // signin widget animation:

    signinWidgetOpacityAnimationControler = AnimationController(
        vsync: this, duration: MODE_CHANGE_ANIMATIONS_DURATION);

    signinWidgetOpacityAnimation = Tween<double>(begin: 1.0, end: 0.0)
        .animate(signinWidgetOpacityAnimationControler)
          ..addListener(update);

    // signup wdget animation:

    signupWidgetOpacityAnimationControler = AnimationController(
        vsync: this, duration: MODE_CHANGE_ANIMATIONS_DURATION);

    signupWidgetOpacityAnimation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(primaryColoredPlatePositionAnimationController)
          ..addListener(update);

    super.initState();
  }

  void update() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.appBarWidget,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height -
            widget.appBarWidget.preferredSize.height,
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Stack(
              children: [
                Positioned(
                  left: primaryColoredPlatePositionAnimation.value *
                      MediaQuery.of(context).size.width,
                  child: _buildPrimaryColoredPlateWidget(context),
                ),
                Positioned(
                  left: signinTextAreaWidgetPositionAnimation.value *
                      MediaQuery.of(context).size.width,
                  child: Visibility(
                    visible: mode == AuthenticationWebScreenMode.SIGNIN_MODE,
                    child: Opacity(
                        opacity: signinTextAreaWidgetOpacityAnimation.value,
                        child: _buildSigninTextAreaWidget(context)),
                  ),
                ),
                Positioned(
                  left: signupTextAreaWidgetPositionAnimation.value *
                      MediaQuery.of(context).size.width,
                  child: Visibility(
                    visible: mode == AuthenticationWebScreenMode.SIGNUP_MODE,
                    child: Opacity(
                        opacity: signupTextAreaWidgetOpacityAnimation.value,
                        child: _buildSignupTextAreaWidget(context)),
                  ),
                ),
                Positioned(
                  left: 0,
                  child: Visibility(
                    visible: mode == AuthenticationWebScreenMode.SIGNUP_MODE,
                    child: Opacity(
                      opacity: signupWidgetOpacityAnimation.value,
                      child: _buildSignupAreaWidget(context),
                    ),
                  ),
                ),
                Positioned(
                  right: 0,
                  child: Visibility(
                    visible: mode == AuthenticationWebScreenMode.SIGNIN_MODE,
                    child: Opacity(
                      opacity: signinWidgetOpacityAnimation.value,
                      child: _buildSigninAreaWidget(context),
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  // builders

  Widget _buildSigninTextAreaWidget(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      height: MediaQuery.of(context).size.height -
          widget.appBarWidget.preferredSize.height,
      child: SigninTextAreaWidget(
        onSignupButtonPressed: () => changeAuthenticationModeToSignup(),
      ),
    );
  }

  Widget _buildSignupTextAreaWidget(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      height: MediaQuery.of(context).size.height -
          widget.appBarWidget.preferredSize.height,
      child: SignupTextAreaWidget(
        onSigninButtonPressed: () => changeAuthenticationModeToSignin(),
      ),
    );
  }

  Widget _buildPrimaryColoredPlateWidget(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      color: Theme.of(context).primaryColor,
      height: MediaQuery.of(context).size.height -
          widget.appBarWidget.preferredSize.height,
    );
  }

  Widget _buildSigninAreaWidget(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width / 8),
      height: MediaQuery.of(context).size.height -
          widget.appBarWidget.preferredSize.height,
      alignment: Alignment.center,
      child: Container(child: SigninWidget()),
    );
  }

  Widget _buildSignupAreaWidget(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width / 8),
      height: MediaQuery.of(context).size.height -
          widget.appBarWidget.preferredSize.height,
      alignment: Alignment.center,
      child: Container(child: SignupWidget()),
    );
  }

  // actions:

  void changeAuthenticationModeToSignin() {
    Future(() {
      primaryColoredPlatePositionAnimationController.reverse();

      signinWidgetOpacityAnimationControler.reverse();
      signupWidgetOpacityAnimationControler.forward();

      signinTextAreaWidgetPositionAnimationControler.reset();
      signinTextAreaWidgetOpacityAnimationControler.reverse();
      signupTextAreaWidgetPositionAnimationControler.forward();
      signupTextAreaWidgetOpacityAnimationControler.reverse();
    }).then((value) => mode = AuthenticationWebScreenMode.SIGNIN_MODE);
  }

  void changeAuthenticationModeToSignup() {
    Future(() {
      primaryColoredPlatePositionAnimationController.forward();

      signinWidgetOpacityAnimationControler.forward();
      signupWidgetOpacityAnimationControler.reverse();

      signinTextAreaWidgetPositionAnimationControler.forward();
      signinTextAreaWidgetOpacityAnimationControler.forward();
      signupTextAreaWidgetPositionAnimationControler.reset();
      signupTextAreaWidgetOpacityAnimationControler.forward();
    }).then((_) => mode = AuthenticationWebScreenMode.SIGNUP_MODE);
  }
}
