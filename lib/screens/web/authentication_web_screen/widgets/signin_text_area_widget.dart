import 'package:flutter/material.dart';
import 'package:myclassesapp/theme/style_modifier.dart';

class SigninTextAreaWidget extends StatelessWidget {
  const SigninTextAreaWidget({Key? key, required this.onSignupButtonPressed})
      : super(key: key);
  final void Function()? onSignupButtonPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: SingleChildScrollView(
        padding: EdgeInsets.all(50.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Sign in",
              style: StyleModifier.textHeadlineWithBackgroundColor(context),
            ),
            SizedBox(height: 50),
            Text(
              "Cras ornare semper leo, non ultricies justo porta non. Aenean eu quam ac arcu ultrices pulvinar. Duis et tellus tincidunt, pellentesque velit commodo, pretium massa. Suspendisse nec iaculis diam, nec rhoncus lacus. Phasellus sollicitudin blandit tortor, et maximus metus. Morbi consequat consequat ex non sagittis",
              style: StyleModifier.textBodyWithBackgroundColor(context),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 100),
            Text(
              "Don't have an account?",
              style: StyleModifier.textBodyWithBackgroundColor(context),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20),
            OutlinedButton(
              onPressed: onSignupButtonPressed,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Sing up",
                  style: StyleModifier.textBodyWithBackgroundColor(context),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
