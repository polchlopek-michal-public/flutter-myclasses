import 'package:flutter/material.dart';
import 'package:myclassesapp/theme/style_modifier.dart';

class SignupTextAreaWidget extends StatelessWidget {
  const SignupTextAreaWidget({Key? key, required this.onSigninButtonPressed}) : super(key: key);
  final void Function() onSigninButtonPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: SingleChildScrollView(
        padding: EdgeInsets.all(50.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Sign up",
              style: StyleModifier.textHeadlineWithBackgroundColor(context),
            ),
            SizedBox(height: 50),
            Text(
              "Cras ornare semper leo, non ultricies justo porta non. Aenean eu quam ac arcu ultrices pulvinar. Duis et tellus tincidunt, pellentesque velit commodo, pretium massa. Suspendisse nec iaculis diam, nec rhoncus lacus. Phasellus sollicitudin blandit tortor, et maximus metus. Morbi consequat consequat ex non sagittis",
              style: StyleModifier.textBodyWithBackgroundColor(context),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 100),
            Text(
              "You have an account?",
              style: StyleModifier.textBodyWithBackgroundColor(context),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20),
            OutlinedButton(
              onPressed: () => onSigninButtonPressed(),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Sing in",
                  style: StyleModifier.textBodyWithBackgroundColor(context),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
