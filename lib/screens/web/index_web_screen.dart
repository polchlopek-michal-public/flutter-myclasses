import 'package:flutter/material.dart';
import 'package:myclassesapp/widgets/authentication/sigin_widget.dart';
import 'package:myclassesapp/widgets/appname_title_widget.dart';
import 'package:myclassesapp/widgets/logo_widget.dart';
import 'package:myclassesapp/widgets/web_appbar_widget/web_appbar_widget.dart';

class IndexWebScreen extends StatefulWidget {
  IndexWebScreen({Key? key}) : super(key: key);

  @override
  _IndexWebScreenState createState() => _IndexWebScreenState();
}

class _IndexWebScreenState extends State<IndexWebScreen> {
  PreferredSizeWidget appBarWidget = WebAppbarWidget();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget,
      body: Container(
          child: ListView(
        children: [
          _buildInformationArea(context),
        ],
      )),
    );
  }

  Widget _buildInformationArea(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height -
          appBarWidget.preferredSize.height,
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(child: _buildInformationWidget(context), flex: 1),
              constraints.maxWidth > 1280
                  ? Expanded(child: _buildCarouselAreaWidget(context), flex: 1)
                  : Expanded(
                      child: Container(
                        alignment: Alignment.center,
                        child: SigninWidget(),
                      ),
                      flex: 0,
                    ),
            ],
          );
        },
      ),
    );
  }

  Widget _buildInformationWidget(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(
                vertical: constraints.maxHeight / 4, horizontal: 100),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AppnameTitleWidget(),
                SizedBox(height: 50),
                Text(
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec arcu a odio placerat sodales ac eget lectus. In at commodo elit. In hendrerit tellus efficitur quam vehicula, vel vehicula sem commodo. Praesent dictum eget mauris eu pellentesque. In elit erat, pharetra ac lectus sed, tincidunt blandit turpis. Praesent pharetra lobortis est vel tincidunt. Morbi nec felis eget quam laoreet vestibulum sed sit amet dolor. Quisque nec accumsan elit, id mollis urna. ",
                  overflow: TextOverflow.clip,
                ),
                SizedBox(height: 50),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                          onPressed: () => Navigator.of(context)
                              .pushNamed("/authentication-screen"),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("Sign in"),
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: OutlinedButton(
                        onPressed: () => Navigator.of(context)
                            .pushNamed("/authentication-screen"),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("Sign up"),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildCarouselAreaWidget(BuildContext context) {
    return Container(
      width: 512,
      height: 512,
      child: LogoWidget(),
    );
  }
}
