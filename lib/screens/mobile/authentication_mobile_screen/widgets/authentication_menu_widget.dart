import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:myclassesapp/widgets/elevated_button_with_image_icon.dart';

class AuthenticationMenuWidget extends StatelessWidget {
  const AuthenticationMenuWidget(
      {Key? key,
      required this.onSigninButtonPressed,
      required this.onSignupButtonPressed,
      required this.onSignWithGoogleButtonPressed})
      : super(key: key);
  final void Function() onSigninButtonPressed;
  final void Function() onSignupButtonPressed;
  final void Function() onSignWithGoogleButtonPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(
              width: double.infinity,
              child: OutlinedButton(
                  onPressed: onSigninButtonPressed,
                  child: Text("Sign in".toUpperCase()))),
          SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                  onPressed: onSignupButtonPressed,
                  child: Text("Sign up".toUpperCase()))),
          __separatorWidget(context),
          SizedBox(
              width: double.infinity,
              child: ElevatedButtonWithIcon(
                  onPressed: onSignWithGoogleButtonPressed,
                  child: Text("Sign with google".toUpperCase()),
                  icon: SvgPicture.asset("assets/icons/google.svg"))),
        ],
      ),
    );
  }

  Widget __separatorWidget(BuildContext context) {
    return Column(children: [
      SizedBox(
        height: 10,
      ),
      Container(
        height: 1,
        color: Theme.of(context).accentColor,
      ),
      Text(
        "or",
        style: TextStyle(color: Colors.white, fontSize: 10),
      ),
      SizedBox(
        height: 2,
      ),
    ]);
  }
}
