import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:myclassesapp/screens/mobile/authentication_mobile_screen/widgets/authentication_menu_widget.dart';
import 'package:myclassesapp/widgets/authentication/sigin_widget.dart';
import 'package:myclassesapp/widgets/authentication/signup_widget.dart';
import 'package:myclassesapp/widgets/appname_title_widget.dart';
import 'package:myclassesapp/widgets/logo_widget.dart';
import 'package:myclassesapp/utils/keyboard_visibility_controller.dart';

enum AuthenticationScreenMode {
  MENU_MODE,
  SIGNIN_MODE,
  SIGNUP_MODE,
}

/// Authentication screen.
/// Reserved only for mobile view due to it's characteristics
/// In this view [AuthenticationMenuWidget], [SignUpWidget] and [SignInWidget]
/// are shown in the samve screen.
///
class AuthenticationMobileScreen extends StatefulWidget {
  AuthenticationMobileScreen({Key? key}) : super(key: key);
  static const double MENU_WIDGET_POSITION_BOTTOM_PROCENTAGE = 0.1;
  static const double LOGO_WIDGET_POSITION_TOP_PROCENTAGE = 0.15;
  static const double SIGNIN_WIDGET_POSITION_BOTTOM_PROCENTAGE = 0.15;
  static const double DEFAULT_PADDING = 40;

  @override
  _AuthenticationMobileScreenState createState() => _AuthenticationMobileScreenState();
}

class _AuthenticationMobileScreenState extends State<AuthenticationMobileScreen>
    with TickerProviderStateMixin {
  // Animation durations consts:
  static const Duration LOGO_ANIMATION_DURATION = Duration(milliseconds: 600);
  static const Duration MENU_ANIMATION_DURATION = Duration(milliseconds: 400);
  static const Duration SIGNIN_ANIMATION_DURATION = Duration(milliseconds: 800);
  static const Duration SIGNUP_ANIMATION_DURATION = Duration(milliseconds: 200);

  // Animations:
  late Animation<double> logoOpacityAnimation;
  late Animation<double> menuOpacityAnimation;
  late Animation<double> menuPositionAnimation;
  late Animation<double> signinWidgetPositionAnimation;
  late Animation<double> signupWidgetOpacityAnimation;

  // Animation controllers:
  late AnimationController logoOpacityAnimationController;
  late AnimationController menuAnyAnimationController;
  late AnimationController signupWidgetOpacityAnimationController;
  late AnimationController signinWidgetPositionAnimationController;

  // Else:
  late AuthenticationScreenMode mode = AuthenticationScreenMode.MENU_MODE;
  late KeyboardVisibilityController keyboardVisibilityController;

  // Pop view related:
  static const Duration SHOULD_POP_COOLDOWN = Duration(seconds: 1);
  bool shouldPopOnBackArrow = false;

  @override
  void initState() {
    // Animation initialziations:
    logoOpacityAnimationController =
        AnimationController(vsync: this, duration: LOGO_ANIMATION_DURATION);
    menuAnyAnimationController =
        AnimationController(vsync: this, duration: MENU_ANIMATION_DURATION);
    signupWidgetOpacityAnimationController =
        AnimationController(vsync: this, duration: SIGNUP_ANIMATION_DURATION);
    signinWidgetPositionAnimationController =
        AnimationController(vsync: this, duration: SIGNIN_ANIMATION_DURATION);

    logoOpacityAnimation = Tween<double>(begin: 1.0, end: 0.1)
        .animate(logoOpacityAnimationController);
    menuOpacityAnimation =
        Tween<double>(begin: 1.0, end: 0.0).animate(menuAnyAnimationController);
    signupWidgetOpacityAnimation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(signupWidgetOpacityAnimationController);
    menuPositionAnimation = Tween<double>(begin: 0, end: 1.0).animate(
      menuAnyAnimationController,
    );
    signinWidgetPositionAnimation = Tween<double>(begin: -1.0, end: 0).animate(
      CurvedAnimation(
        parent: signinWidgetPositionAnimationController,
        curve: Curves.bounceOut,
      ),
    );

    keyboardVisibilityController = KeyboardVisibilityController(
      context: context,
      onKeyboardDismiss: onKeyboardDismiss,
      onKeyboardReveal: onKeyboardReveal,
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onBackButtonPressed,
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: MediaQuery.of(context).orientation == Orientation.portrait
            ? buildPortraitView(context)
            : buildLandscapeView(context),
      ),
    );
  }

  Widget buildPortraitView(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return Stack(
          alignment: Alignment.center,
          children: [
            buildAnimatedLogoWithAppnameWidget(context,
                Size(constraints.maxWidth * 0.8, constraints.maxHeight)),
            buildMenuWidget(
                context, Size(constraints.maxWidth, constraints.maxHeight)),
            buildSigninWidget(
                context, Size(constraints.maxWidth, constraints.maxHeight)),
            buildSignupWidget(
                context, Size(constraints.maxWidth, constraints.maxHeight)),
          ],
        );
      }),
    );
  }

  Widget buildLandscapeView(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width * 0.45,
              child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  return Container(
                    padding: EdgeInsets.only(
                      top: 20,
                    ), //! can cause overflow, because column of signup's widgets can be higher than this container hight minus padding!
                    child: Stack(
                      children: [
                        buildAnimatedAppnameWidget(context),
                        buildMenuWidget(context,
                            Size(constraints.maxWidth, constraints.maxHeight)),
                        buildSigninWidget(context,
                            Size(constraints.maxWidth, constraints.maxHeight)),
                        buildSignupWidget(context,
                            Size(constraints.maxWidth, constraints.maxHeight)),
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
          buildStaticLogoWidget(context, Size(300, 300)),
        ],
      ),
    );
  }

  // >>> Widget builders <<<

  Widget buildAnimatedLogoWithAppnameWidget(BuildContext context, Size size) {
    return AnimatedBuilder(
        animation: logoOpacityAnimationController,
        builder: (BuildContext context, Widget? child) {
          return Positioned(
            top: size.height *
                AuthenticationMobileScreen.LOGO_WIDGET_POSITION_TOP_PROCENTAGE,
            child: Opacity(
              opacity: logoOpacityAnimation.value,
              child: Stack(
                alignment: Alignment.topCenter,
                children: [
                  LogoWidget(
                    width: size.width,
                    height: size.width,
                  ),
                  Positioned(
                    bottom: size.width * 0.2,
                    child: AppnameTitleWidget(),
                  ),
                ],
              ),
            ),
          );
        });
  }

  Widget buildAnimatedAppnameWidget(BuildContext context) {
    return Positioned(
      child: AnimatedBuilder(
          animation: logoOpacityAnimationController,
          builder: (BuildContext context, Widget? child) {
            return Opacity(
              opacity: logoOpacityAnimation.value,
              child: AppnameTitleWidget(),
            );
          }),
    );
  }

  Widget buildStaticLogoWidget(BuildContext context, Size size) {
    return LogoWidget(
      width: size.width,
      height: size.height,
    );
  }

  Widget buildMenuWidget(BuildContext context, Size size) {
    return AnimatedBuilder(
      animation: menuAnyAnimationController,
      builder: (BuildContext context, Widget? child) {
        return Positioned(
          left: menuPositionAnimation.value * size.width,
          bottom: size.height *
              AuthenticationMobileScreen.MENU_WIDGET_POSITION_BOTTOM_PROCENTAGE,
          child: Opacity(
            opacity: menuOpacityAnimation.value,
            child: Container(
              padding: EdgeInsets.all(AuthenticationMobileScreen.DEFAULT_PADDING),
              width: size.width,
              child: AuthenticationMenuWidget(
                onSigninButtonPressed: onSigninButtonPressed,
                onSignupButtonPressed: onSignupButtonPressed,
                onSignWithGoogleButtonPressed: onSignWithGoogleButtonPressed,
              ),
            ),
          ),
        );
      },
    );
  }

  Widget buildSigninWidget(BuildContext context, Size size) {
    return AnimatedBuilder(
      animation: signinWidgetPositionAnimationController,
      builder: (BuildContext context, Widget? child) {
        return Positioned(
          left: signinWidgetPositionAnimation.value * size.width,
          bottom: size.height *
              AuthenticationMobileScreen.SIGNIN_WIDGET_POSITION_BOTTOM_PROCENTAGE,
          child: Container(
            padding: EdgeInsets.all(AuthenticationMobileScreen.DEFAULT_PADDING),
            width: size.width,
            child: SigninWidget(),
          ),
        );
      },
    );
  }

  Widget buildSignupWidget(BuildContext context, Size size) {
    return AnimatedBuilder(
      animation: signupWidgetOpacityAnimationController,
      builder: (BuildContext context, Widget? child) {
        return Positioned.fill(
          child: Visibility(
            visible: mode == AuthenticationScreenMode.SIGNUP_MODE,
            child: Opacity(
              opacity: signupWidgetOpacityAnimation.value,
              child: Container(
                height: size.height,
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(
                    horizontal: AuthenticationMobileScreen.DEFAULT_PADDING),
                child: SignupWidget(),
              ),
            ),
          ),
        );
      },
    );
  }

  // >>> Action handlers: <<<

  void onSigninButtonPressed() {
    mode = AuthenticationScreenMode.SIGNIN_MODE;
    menuAnyAnimationController.forward();
    signinWidgetPositionAnimationController.forward();
  }

  void onSignupButtonPressed() {
    mode = AuthenticationScreenMode.SIGNUP_MODE;
    menuAnyAnimationController.forward();
    signupWidgetOpacityAnimationController.forward();
    logoOpacityAnimationController.forward();
  }

  void onSignWithGoogleButtonPressed() {
    //TODO: fill when Google signin functionality will be fullfill
  }

  void onArrowBackPressed() {
    mode = AuthenticationScreenMode.MENU_MODE;
    hideScreenKeyboard();
    Future.delayed(Duration(milliseconds: 100))
        .then((_) => resetAllAnimations());
  }

  void onKeyboardDismiss() {
    if (mode != AuthenticationScreenMode.SIGNUP_MODE) {
      if (logoOpacityAnimation.status == AnimationStatus.completed) {
        logoOpacityAnimationController.reverse();
      }
    }
  }

  void onKeyboardReveal() {
    if (logoOpacityAnimation.status != AnimationStatus.completed &&
        logoOpacityAnimation.status != AnimationStatus.forward) {
      logoOpacityAnimationController.forward();
    }
  }

  Future<bool> onBackButtonPressed() async {
    // when view is in MENU_MODE
    if (mode == AuthenticationScreenMode.MENU_MODE) {
      if (shouldPopOnBackArrow) {
        return true;
      } else {
        // show snackbar and set [shouldPopOnBackArrow]
        shouldPopOnBackArrow = true;
        ScaffoldMessenger.of(context).showSnackBar(
          buildClickAgainToExitSnackBar(context),
        );
        Future.delayed(SHOULD_POP_COOLDOWN)
            .then((_) => shouldPopOnBackArrow = false);
      }
    }

    // when view is NOT in MENU_MODE
    mode = AuthenticationScreenMode.MENU_MODE;
    hideScreenKeyboard();
    Future.delayed(Duration(milliseconds: 100))
        .then((_) => resetAllAnimations());
    return false;
  }

  SnackBar buildClickAgainToExitSnackBar(BuildContext context) {
    return SnackBar(
      elevation: 6.0,
      behavior: SnackBarBehavior.floating,
      content: Container(
        child: Text(
          "Click again to exit myclasses",
          textAlign: TextAlign.center,
        ),
      ),
      duration: SHOULD_POP_COOLDOWN,
    );
  }

  // >>> Helpers: <<<

  /// Helps to reverse initial state of screen.
  /// All widget animated states all reset immediately,
  /// instead of animation of logo opacity which are reveres when necessery.
  void resetAllAnimations() {
    menuAnyAnimationController.reset();
    signinWidgetPositionAnimationController.reset();
    signupWidgetOpacityAnimationController.reset();
    if (logoOpacityAnimation.isCompleted) {
      logoOpacityAnimationController.reverse();
    }
  }

  /// Hides screen keyboard
  void hideScreenKeyboard() {
    keyboardVisibilityController.hideKeyboard();
  }

  @override
  void dispose() {
    keyboardVisibilityController.dispose();
    super.dispose();
  }
}
