import 'package:flutter/material.dart';
import 'package:myclassesapp/config/backend_config.dart';
import 'package:myclassesapp/errors/config_error.dart';
import 'package:yaml/yaml.dart' as yaml;

class ConfigProvider {
  ConfigProvider._();
  static ConfigProvider _provider = ConfigProvider._();

  static Future<void> initialize(BuildContext context) async {
    return await _provider.init(context);
  }

  static ConfigProvider provide() {
    if (!_provider.initialized) {
      throw ConfigUninitializedError();
    }
    return _provider;
  }

  // initialziation flag
  bool initialized = false;

  // configurations
  late BackendConfig _backend;

  // initialziation function

  Future<void> init(BuildContext context) async {
    // getting config's raw data
    String rawData =
        await DefaultAssetBundle.of(context).loadString("assets/config/config.yaml");
    dynamic yamlConfig = yaml.loadYaml(rawData);

    // inititializing backend config
    _backend = initBackendConfig(yamlConfig["backend"]);

    // set initialization flag
    initialized = true;
  }

  // initialization helpers

  BackendConfig initBackendConfig(dynamic backendYamlConfig) {
    return BackendConfig(
      address: backendYamlConfig["address"],
      port: backendYamlConfig["port"],
      responseTimeout: Duration(
        milliseconds: backendYamlConfig["response_timeout_ms"],
      ),
    );
  }

  // config getters

  ConfigType _config<ConfigType>(ConfigType config) {
    if (!initialized) {
      throw ConfigUninitializedError();
    }
    return config;
  }

  BackendConfig backend() {
    return _config(_backend);
  }
}
