class BackendConfig {
  BackendConfig({required this.address, required this.port, required this.responseTimeout});

  String address;
  int port;
  Duration responseTimeout;
}
