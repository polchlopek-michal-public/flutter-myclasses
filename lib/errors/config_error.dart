import 'package:myclassesapp/errors/error.dart';

class ConfigUninitializedError extends MyClassesException {
  ConfigUninitializedError()
      : super(message: "Config can't be get becouse config isn't initialzied");
}
