import 'package:myclassesapp/errors/error.dart';
import 'package:myclassesapp/managers/session_manager/session_status.dart';

class IncorrectSessionStatusError extends MyClassesException {
  IncorrectSessionStatusError(
      {message = "", required this.actual, required this.expected})
      : super(message: message);

  SessionStatus actual;
  SessionStatus expected;
}
