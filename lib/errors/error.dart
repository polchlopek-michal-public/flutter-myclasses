class MyClassesException implements Exception {
  MyClassesException({this.message});

  final message;
}
