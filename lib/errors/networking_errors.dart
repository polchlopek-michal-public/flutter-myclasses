import 'error.dart';

class NetworkingFetchDataError extends MyClassesException {
  NetworkingFetchDataError(String message) : super(message: message);
}

class NetworkingBadRequestError extends MyClassesException {
  NetworkingBadRequestError(message) : super(message: message);
}

class NetworkingUnauthorizedError extends MyClassesException {
  NetworkingUnauthorizedError(message) : super(message: message);
}
