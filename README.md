```
___  ___      _____ _                          
|  \/  |     /  __ \ |                         
| .  . |_   _| /  \/ | __ _ ___ ___  ___  ___  
| |\/| | | | | |   | |/ _` / __/ __|/ _ \/ __| 
| |  | | |_| | \__/\ | (_| \__ \__ \  __/\__ \ 
\_|  |_/\__, |\____/_|\__,_|___/___/\___||___/ 
         __/ |                                 
        |___/                                  
___  ___      _     _ _       ___              
|  \/  |     | |   (_) |     / _ \             
| .  . | ___ | |__  _| | ___/ /_\ \_ __  _ __  
| |\/| |/ _ \| '_ \| | |/ _ \  _  | '_ \| '_ \ 
| |  | | (_) | |_) | | |  __/ | | | |_) | |_) |
\_|  |_/\___/|_.__/|_|_|\___\_| |_/ .__/| .__/ 
                                  | |   | |    
                                  |_|   |_|    

```

# **Description**

MyClassess project mobile app realized for practicing flutter animation skills.  

# **Build and run**

## **Build using docker**

### **Pre-requirements:**  
* Executed in project's direcotry.  
* Delete `build/` and `pubspec.lock` using `sudo` if necessary. 

### **Build docker image:**  
```
docker build -t myclassesapp .
```   
### **Build Mobile app:**    
```
docker run -it -v ${PWD}:/workspace myclassesapp /bin/bash -c "/workspace/build.sh"
```  
### **Build output path:**  
`mobileapp/build/app/outputs/apk/release/app-release.apk`  

### **Run instructions:**   
Copy `.apk` file from output path to android smarthphone device or simulator. Then install it and launch.

</br>  

# Screenshots
![](docs/output.gif)