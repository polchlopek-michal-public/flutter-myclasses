FROM ubuntu:20.04

RUN apt-get update
RUN apt-get install -y android-sdk
RUN apt-get install -y wget
RUN apt-get install -y unzip
RUN apt-get install -y git
RUN apt-get install -y curl

RUN apt-get install -y clang cmake \
    -y ninja-build \
    -y pkg-config \
    -y libgtk-3-dev

RUN wget https://dl.google.com/android/repository/commandlinetools-linux-6609375_latest.zip
ENV ANDROID_HOME=/usr/lib/android-sdk
RUN unzip commandlinetools-linux-6609375_latest.zip -d cmdline-tools
RUN mv cmdline-tools $ANDROID_HOME/
ENV PATH=$ANDROID_HOME/cmdline-tools/tools/bin:$PATH

RUN yes | sdkmanager --install "system-images;android-29;google_apis;x86_64"
RUN yes | sdkmanager --install "platforms;android-29"
RUN yes | sdkmanager --install "build-tools;29.0.2"

RUN yes | sdkmanager --install "system-images;android-30;google_apis;x86_64"
RUN yes | sdkmanager --install "platforms;android-30"
RUN yes | sdkmanager --install "build-tools;30.0.3"

RUN yes | sdkmanager --licenses

RUN git clone https://github.com/flutter/flutter.git
ENV PATH=$PATH:/flutter/bin
RUN  flutter config --enable-linux-desktop
RUN  flutter doctor

RUN mkdir -p /workspace
